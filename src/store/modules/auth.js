const state = {
    auth: {
        isAuthentication: true
    }
}

const getters = {
    isAuthentication: state => state.auth.isAuthentication
}

const mutations = {
    TOGGLE_AUTH(state) {
        state.auth.isAuthentication = !state.auth.isAuthentication
    }
}

export default {state, getters, mutations}
