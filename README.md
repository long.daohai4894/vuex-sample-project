# Vuex-project
> Sample project for how to use vuex in VueJS

## Documentation
[Vuex Document](https://hackmd.io/@AY7COwqNTkWyfqaGESUABg/ryiVzdjWd)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
